var express = require('express');
var router = express.Router();
var indexController = require("../controllers/indexController")
const { body, validationResult } = require('express-validator');

/* GET home page */
router.get('/', indexController.index);
// GET blog page
router.get('/blog', indexController.blog);
router.get('/article/:id', indexController.getArticleById);
router.get('/blog-actualites', indexController.getArticleByActualites);
router.get('/blog-conseils', indexController.getArticleByConseils);
router.get('/blog-Referencement', indexController.getArticleByReferencement);
router.get('/blog-analytics', indexController.getArticleByAnalytics);
router.get('/blog-web', indexController.getArticleByWeb);
router.get('/blog-wg', indexController.getArticleByWG);
router.get('/blog-mc', indexController.getArticleByMC);
router.get('/blog-hb', indexController.getArticleByHB);
router.get('/blog-janv', indexController.getArticleBy01);
router.get('/blog-fevr', indexController.getArticleBy02);
router.get('/blog-mars', indexController.getArticleBy03);
router.get('/blog-avr', indexController.getArticleBy04);
router.get('/blog-mai', indexController.getArticleBy05);
router.get('/blog-juin', indexController.getArticleBy06);
router.get('/blog-juil', indexController.getArticleBy07);
router.get('/blog-aout', indexController.getArticleBy08);
router.get('/blog-sept', indexController.getArticleBy09);
router.get('/blog-oct', indexController.getArticleBy10);
router.get('/blog-nov', indexController.getArticleBy11);
router.get('/blog-dec', indexController.getArticleBy12);
// GET services page
router.get('/services', indexController.services);
router.get('/detailservice/:id', indexController.getDetailServiceById);
// GET à propo page
router.get('/propos', indexController.propos);
// GET Contact page
router.get('/contact', indexController.contact);
//router.post('/contact-send', indexController.sendContact)

router.post('/contact-send',
    body('fullname', "Invalide").notEmpty(),
    body('email', "Invalide").notEmpty().isEmail(),
    body('numTel', "Invalide").notEmpty().isMobilePhone(),
    body('message', "Invalide").notEmpty().isLength({max:300}), indexController.sendContact );

module.exports = router;
