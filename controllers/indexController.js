const { ArticleModel } = require('../models/article');
const { ServiceModel } = require('../models/service');
const { LicenceModel } = require('../models/licence');
const { PropoModel } = require('../models/propos');
const { TeamMateModel } = require('../models/teamMate');
const { ContactModel } = require('../models/contact');
const { body, validationResult } = require('express-validator');

exports.index = async (req, res, next) => {
    try {
        let document = await ServiceModel.find({});
        let document2 = await LicenceModel.find({});
        res.render('../views/index', { title: 'Force Majeure', sous_titre: "L’outil d’optimisation pour moteurs de recherche le plus performant sur le marché!" , licences: document2 ,services: document});
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured index");
    }
}
// Page blog
exports.blog = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({});
        res.render('../views/blog', { title: 'Blog', articles: document });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured blog");
    }
}
exports.getArticleById = async (req, res, next) => {
    try {
        let document = await getArticleById(req.params.id);
        res.render('../views/article', { title: 'Blog', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
// Filter blog page
exports.getArticleByActualites = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({category: "Actualites"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par catégorie: Actualités', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleByConseils = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({category: "Conseils"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par catégorie: Conseils', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleByReferencement = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({category: "Referencement"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par catégorie: Referencement', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleByAnalytics = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({category: "Analytics"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par catégorie: Analytics', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleByWeb = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({category: "Web"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par catégorie: Web', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
// Filter author
exports.getArticleByWG = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({author: "William Gauvin"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par auteur: Hans', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleByMC = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({author: "Mathieu Cossette"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par auteur: Hans', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleByHB = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({author: "Hans Bherer"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par auteur: Hans', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
// Filter by date
exports.getArticleBy01 = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({month: "01"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par mois: Janvier', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleBy02 = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({month: "02"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par mois: Février', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleBy03 = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({month: "03"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par mois: Mars', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleBy04 = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({month: "04"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par mois: Avril', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleBy05 = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({month: "05"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par mois: Mai', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleBy06 = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({month: "06"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par mois: Juin', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleBy07 = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({month: "07"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par mois: Juillet', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleBy08 = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({month: "08"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par mois: Août', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleBy09 = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({month: "09"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par mois: Septembre', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleBy10 = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({month: "10"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par mois: Octobre', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleBy11 = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({month: "11"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par mois: Novembre', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}
exports.getArticleBy12 = async (req, res, next) => {
    try {
        let document = await ArticleModel.find({month: "12"}).exec();
        res.render('../views/blog', { title: 'Blog', filter: 'Filtre par mois: Décembre', articles: document });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}


// Page service
exports.services = async (req, res, next) => {
    try {
        let document = await ServiceModel.find({});
        res.render('../views/services', { title: 'Nos services', services: document });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured marketing");
    }
}
exports.getDetailServiceById = async (req, res, next) => {
    try {
        let document = await getDetailServiceById(req.params.id);
        let document2 = await LicenceModel.find({});
        res.render('../views/detailservice', { title: 'Nos Services', services: document, licences: document2 });
    } catch ( err ) {
        console.log(err);
        res.status(500).send("Internal Server error Occured");
    }
}


// Page À propo
exports.propos = async (req, res, next) => {
    try {
        let document = await PropoModel.find({})
        let document2 = await TeamMateModel.find({})
        res.render('../views/propos', { title: 'A propos', propos: document, teammates: document2 });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured propos");
    }
}



// Page Contact
exports.contact = async (req, res, next) => {
    try {
        res.render('../views/contact', { title: 'Contact' });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured contacter");
    }
}
exports.sendContact = async (req, res, next) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.render('../views/contact', { title: 'Contact', msg: 'VOS DONNÉES ENTRÉES SONT INVALIDES!' });
        }
        let data = new ContactModel(req.body);
        data.save(function (err, data) {
            res.render('../views/contact', { title: 'Contact', msg: 'DEMANDE DE CONTACT ENVOYÉE! NOUS VOUS CONTACTERONS DANS LES PLUS BREF DÉLAIS!' });
        })
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server error Occured ADD_ARTICLE");
    }
}

const getArticleById = async (id) =>{
    return await ArticleModel.findById(id);
}

const getDetailServiceById = async (id) =>{
    return await ServiceModel.findById(id);
}
